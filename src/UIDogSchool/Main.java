/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UIDogSchool;

//Importacion de clases 
import DogSchool.DogSchool;
import DogSchool.Dueno;
import DogSchool.Perro;
import java.util.Scanner;

/**
 *
 * @author Andres vargas
 */
public class Main {

    DogSchool aplicacion = new DogSchool();
    static Scanner sc = new Scanner(System.in);

    /**
     * @param args the command line arguments
     * @throws java.lang.InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {

        //Llamada menu
        Main main = new Main();
        main.menu();
    }
    
// Menu, que pide una opcion para poder realizar una accion.
    public void menu() throws InterruptedException {
        String opcion = "";
        while (!opcion.equals("6")) {
            System.out.println("╔                Menu                       ╗");
            System.out.println("║ 1. Registro de Duenos                     ║");
            System.out.println("║ 2. Registro de Perros                     ║");
            System.out.println("║ 3. Consulta de Perros   por Dueno         ║");
            System.out.println("║ 4. Consulta de Perros   por Raza          ║");
            System.out.println("║ 5. Consulta/RemoverMascotas por Codigo    ║");
            System.out.println("║ 6. Salir de DogSchool                     ║");
            System.out.println("╚                                           ╝");
            System.out.print("Ingrese opcion: ");
            opcion = sc.nextLine();
            switch (opcion) {
                case "1":
                    registroDuenos();
                    break;
                case "2":
                    registroMascotas();
                    break;
                case "3":
                    consultaMascotasPorDueno();
                    break;
                case "4":
                    consultaMascotasPorRaza();
                    break;
                case "5":
                    System.out.println("1. Consultar mascota\n2.Remover mascota\n3.Salir");
                    System.out.println("Ingrese opcion: ");
                    String op;
                    op = sc.nextLine();
                    while(Integer.parseInt(op)>2){
                        
                        if (!op.equals("1"))
                            remueveMascota();
                        else
                            consultaMascotasPorCodigo();
                        
                    }
                    break;

                default:
                    System.out.println("Opcion No valida!!");
                    
            }
        }
        sc.close();
    }
//Registra a los dueños de las mascotas
    public void registroDuenos() {
        System.out.print("Ingresar cedula: ");
        String cedula = sc.nextLine();
        System.out.print("Ingresar nombre: ");
        String nombre = sc.nextLine();
        System.out.print("Ingresar ciudad: ");
        String ciudad = sc.nextLine();
        System.out.print("Ingresar direccion: ");
        String direccion = sc.nextLine();
        System.out.print("Desea registrar Tarjeta de Credito ahora S/N: ");
        String opcion = sc.nextLine();
        Dueno d1;
        if (opcion.equalsIgnoreCase("S")) {
            System.out.print("Ingresar tarjeta credito: ");
            long tarjeta = sc.nextLong();
            /**
             * TODO: Crear objeto owner usando el constructor correspondiente
             */
            d1 = new Dueno(nombre, ciudad, direccion, cedula, tarjeta);

        } else {
            /*
               TODO: Crear objeto owner usando el constructor correspondiente             
             */
            d1 = new Dueno(nombre, ciudad, direccion, cedula);

        }
        aplicacion.anadirDueno(d1);
        System.out.println("Se registro exitosamente!");

    }
//registra las mascotas
    public void registroMascotas() {
        System.out.print("Ingresar cedula dueno: ");
        String cedula = sc.nextLine();
        Perro p1;
        Dueno d = aplicacion.consultarDueno(cedula);
        if (d == null) {
            System.out.println("NO existe cédula asociada!");
        } else {
            System.out.println("Nombre de dueño: " + d.getNombre());
            System.out.print("Ingrese nombre: ");
            String nombre = sc.nextLine();
            System.out.print("Ingrese raza: ");
            String raza = sc.nextLine();
            System.out.print("Se registro exitosamente!");
            p1 = new Perro(nombre, raza, d);
            System.out.println("La mascota con el codigo: " + p1.getidPerro());
            aplicacion.anadirPerro(p1);
        }
        

    }
//consulta las mascotas enviandole una cedula
    public void consultaMascotasPorDueno() {
        System.out.print("Ingresar cedula dueno: ");
        String cedula = sc.nextLine();
        Dueno d2 = aplicacion.consultarDueno(cedula);
        
        if (d2 == null) {

            System.out.println("NO EXISTE");

        }
        else{
            
            aplicacion.consultaPerroD(d2).forEach((p) -> {
                System.out.println(p.getidPerro() + "--" + p.getNombre() + "-Dueno: " + p.getDueno().getNombre());
            });
        }
            

    }
//consultara las mascotas por la raza enviada
    public void consultaMascotasPorRaza() {
        System.out.print("Ingresar raza: ");
        String raza = sc.nextLine();
        if (aplicacion.consultaPerroR(raza).isEmpty())
            System.out.println("NO HAY");
        else{
        aplicacion.consultaPerroR(raza).forEach((Perro p) -> {
            System.out.println(p.getidPerro() + "--" + p.getNombre() + "-Dueno: " + p.getDueno().getNombre());
        });
        }

    }
//consulata las mascotas por el codigo enviado
    public void consultaMascotasPorCodigo() {
        System.out.print("Ingresar codigo: ");
        int codigo = sc.nextInt();
        Perro p;
        p = aplicacion.consultaPerroC(codigo);
        if (p!=null){
            System.out.println(p.getidPerro() + "--" + p.getNombre() + "-Dueno: " + p.getDueno().getNombre());

        }
        else
            System.out.println("NO EXISTE");

    }
    public void remueveMascota() throws InterruptedException{
        System.out.println("Ingrese codigo del perro que desea remover: ");
        int codigo = sc.nextInt();
        Perro p;
        p = aplicacion.consultaPerroC(codigo);
        if (p!=null){
            System.out.println(p.getidPerro() + "--" + p.getNombre() + "-Dueno: " + p.getDueno().getNombre());
            System.out.println("Esta seguro de que desea eliminarlo? S/N");
            String resp = sc.nextLine();
            if (resp.equalsIgnoreCase("S")){
                aplicacion.remuevePerro(p);
                System.out.println("El perro ha sido removido con exito :'(");
            }
            System.out.println("Volverá al menu principal");
            Thread.sleep(2000);
        }
        else
            System.out.println("NO EXISTE");
    }

}
/*1.- En la tarea 3: ¿Por qué no se puede acceder directamente a las listas Perros y Duenos del objeto “aplicacion” en la clase Main?
Debido a que estaban declarados como privados. Es decir estában encapsulados

2.- En la tarea 3: Si quisiéramos acceder directamente a las listas Perros y Duenos del objeto “aplicacion” en la clase Main, ¿qué cambios deberíamos hacer? ¿Sería esta acción recomendable?
Facilomente se pueden declarar como publicas, pero no sería recomendable ya que no cumpliría con los parametros de POO. Otra opcion es usar metodos getter y setter.

3.-Investiga el concepto de sobrecarga. ¿En qué aspectos de este taller aplicamos el concepto de Sobrecarga? ¿Por qué utilizamos sobrecarga?
Existen sobrecargas de metodos y de constructores. En este taller se hace uso de la sobrecargas de constructores.
Se utiliza la sobrecarga en la clase Dueno. Debido a que se este objeto puede ser creado ciertos atributos o con otros atributos, dependiendo 
de la nesecidad del usuario.
**/