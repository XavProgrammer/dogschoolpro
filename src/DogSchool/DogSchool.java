/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DogSchool;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *Clase DogSchool que contiene todos los metodos y constructores de los objetos Dogschool
 * @author Andrés Vargas
 */

public class DogSchool {
//inicializo listas
    private List<Perro> perros = new ArrayList<>();
    private List<Dueno> duenos = new ArrayList<>();

    /**
     * TODO: Anadir una nueva mascota a la lista de mascotas
     *
     * @param perro
     */
    public void anadirPerro(Perro perro) {
        perros.add(perro);

    }

    /**
     * TODO: Anadir un nuevo dueno a la lista de duenos
     *
     * @param dueno
     */
//anade un dueno
    public void anadirDueno(Dueno dueno) {
        duenos.add(dueno);
    }
//consulta si el dueno existe
    public Dueno consultarDueno(String cedula) {
        return duenos.stream().filter(e -> e.getCedula().equals(cedula)).findAny().orElse(null);

    }
    //devuelve una lista con los perros
    public List<Perro> consultaPerroD(Dueno dueno){
        return perros.stream().filter(p -> p.getDueno().equals(dueno)).collect(Collectors.toList());
    }
    public Perro consultaPerroC(int codigo){
        return perros.stream().filter(p -> p.getidPerro()==(codigo)).findAny().orElse(null);
    }
    //devuelve una lista con los peros que cumplen
    public List<Perro> consultaPerroR(String raza){
        
        return perros.stream().filter(p -> p.getRaza().equals(raza)).collect(Collectors.toList());
    }

    public void remuevePerro(Perro p) {
        perros.remove(p);
    }
    

}
