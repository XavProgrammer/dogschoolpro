/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DogSchool;

import java.util.Random;

/**
 *Clase Pero que contiene los metodos y constructores
 * @author Andrés Vargas 
 */
public class Perro {

    /**
     * Metodo llamado desde el constructor para generar codigos a las mascotas ingresadas
     * @return Codigo entero aleatorio para las mascotas creadas
     */
        private int obtieneCodigo(){
        Random r = new Random();
        return r.nextInt(1500);
                       
    }
    private int idPerro;
    private String nombre, raza;
    private Dueno dueno;
    /**
     * Constructor de la Clase Perro
     * @param nombre
     * @param raza
     * @param dueno 
     */
    public Perro(String nombre, String raza, Dueno dueno){
        this.nombre = nombre;
        this.raza = raza;
        this.dueno = dueno;
        this.idPerro= obtieneCodigo();
    }
    /**
     * Setter de nombre
     * @param nombre 
     */
    public void setNombre(String nombre){
        this.nombre = nombre;
    }/**
     * Setter de la raza del Obejeto Perro
     * @param raza 
     */
    public void setRaza(String raza){
        this.raza = raza;
    }
    /**
     * Setter del dueno del perro
     * @param dueno 
     */
    public void setDueno(Dueno dueno){
        this.dueno = dueno;
    }
    //getter
    /**
     * Getter del nombre del Perro
     * @return 
     */
    public String getNombre(){
        return this.nombre;
    }
    /**
     * Getter de la raza del Perro
     * @return 
     */
    public String getRaza(){
        return this.raza;
    }
    /**
     * Detter del dueno del Perro
     * @return 
     */
    public Dueno getDueno(){
        return this.dueno;
    }
    /**
     * Getter del codigo del Pero
     * @return 
     */

    public int getidPerro() {
        return this.idPerro;
    }

    
  
}
