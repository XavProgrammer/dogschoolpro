/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DogSchool;

/**
 *Clase Dueno que contiene constructores y metodos propios de la clase
 * @author Andrés Vargas
 */
public class Dueno {
    
    private String nombre, ciudad, cedula, direccion;
    private long tarjeta;
    /**
     * Constructor de la Clase Dueno
     * @param nombre
     * @param ciudad
     * @param direccion
     * @param cedula 
     */
    public Dueno(String nombre, String ciudad, String direccion, String cedula){
        this.nombre = nombre;
        this.ciudad = ciudad;
        this.direccion = direccion;
        this.cedula = cedula;    
        
    }
    /**
     * Sobrecarga de constructor, el cual se le agrega el parametro tarjeta
     * @param nombre
     * @param ciudad
     * @param direccion
     * @param cedula
     * @param tarjeta 
     */
    public Dueno(String nombre, String ciudad, String direccion, String cedula, long tarjeta){
        this.nombre = nombre;
        this.ciudad = ciudad;
        this.direccion = direccion;
        this.cedula = cedula;
        this.tarjeta = tarjeta;
    }
    //setters
    /**
     * Setter del nombre del dueno
     * @param nombre 
     */
    public void setNombre(String nombre){
        this.nombre=nombre;
    }
    /**
     * Setter de la ciudad en la que vive el dueño
     * @param ciudad 
     */
    public void setCiudad(String ciudad){
        this.ciudad = ciudad;
    }
    /**
     * Setter de la direccion de domicilio del dueno
     * @param direccion 
     */
    public void setDireccion(String direccion){
        this.direccion = direccion;
    }
    /**
     * Setter de la cédula del dueno
     * @param cedula 
     */
    public void setCedula(String cedula){
        this.cedula = cedula;
    }
    
    //getters
    /**
     * Getter de devuelve el nombre del dueno
     * @return 
     */
    public String getNombre(){
        return this.nombre;
    }
    /**
     * Getter que devuelve la ciudad del dueno
     * @return 
     */
    public String getCiudad(){ 
        return this.ciudad;
    }
    /**
     * Getter que devuelve la direccion del dueno
     * @return 
     */
    public String getDireccion(){
        return direccion;
    }
    /**
     *getter que devuelve la cedula del dueno     * 
     * @return 
     */
    public String getCedula(){
        return this.cedula;
    }
    
    
    
}
